import unittest
import functions


class TestCardCVV(unittest.TestCase):
    
    # Check for any input if it's not empty
    def test_inputIsNotEmpty(self):
        self.assertFalse(functions.inputIsNotEmpty(''))
        
    # Check if CVV is exactly three chars
    def test_CVVIsThreeChars(self):
        self.assertTrue(functions.verifyCVV("158"))
        self.assertFalse(functions.verifyCVV("15889"))
        self.assertFalse(functions.verifyCVV(""))
    
    # Check if CVV is only numbers
    def test_CVVIsNumbers(self):
        self.assertTrue(functions.verifyCVV("469"))
        self.assertFalse(functions.verifyCVV("t $"))
    


if __name__ == '__main__':
    unittest.main()
