import unittest
import functions


class TestCardNumber(unittest.TestCase):
    
    # Check for any input if it's not empty
    def test_inputIsNotEmpty(self):
        self.assertFalse(functions.inputIsNotEmpty(''))

    # Check if the card number lentgh is exactly 16
    def test_cardNumberLength(self):
        self.assertTrue(functions.verifyCardNumber("4373097732700163"))
        self.assertFalse(functions.verifyCardNumber("12"))
        self.assertFalse(functions.verifyCardNumber("1234567812345678915454"))

    # Check if the card number is only made of numbers
    def test_cardNumberIsNumbers(self):
        self.assertFalse(functions.verifyCardNumber("é&$ù^]*+azertyui"))

    # Check if there is no spaces in the card number
    def test_cardNumberSpaces(self):
        self.assertFalse(functions.verifyCardNumber("é&$ù   +aze   ui"))

    # Check if the card number is correct in regard of the Luhn Algorythm
    def test_cardNumberLuhnValidity(self):
        self.assertTrue(functions.verifyCardNumber("4373097732700163"))
        self.assertFalse(functions.verifyCardNumber("4563099632700163"))
    


if __name__ == '__main__':
    unittest.main()
