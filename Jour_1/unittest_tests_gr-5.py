import unittest
from Function_groupe4 import sentenceVerification


class TestSentence(unittest.TestCase):
    # Test if there's an uppercase at the start of the string
    def test_firstLetterUpperCase(self):
        self.assertTrue(sentenceVerification.sentenceVerification("Ceci est une phrase."))
        self.assertFalse(sentenceVerification.sentenceVerification("ceci est une phrase."))

    # Test if the number of words is between 2 and 10 excluded
    def test_numberOfWords(self):
        self.assertFalse(sentenceVerification.sentenceVerification("Ceci est."))
        self.assertTrue(sentenceVerification.sentenceVerification("Ceci est une phrase."))
        self.assertFalse(sentenceVerification.sentenceVerification("Ceci est une grande phrase trop longue pour être valide."))

    # Test if there's a period at the end of the sentence
    def test_periodAtEnd(self):
        self.assertTrue(sentenceVerification.sentenceVerification("Ceci est une phrase."))
        self.assertFalse(sentenceVerification.sentenceVerification("Ceci est une phrase"))


    # GROUP 5 EDITS
    # Verify if there is only numbers in the setence
    def test_onlyNumbers(self):
        self.assertTrue(sentenceVerification.sentenceVerification("Il n'y a pas 8531 que des 44643 nombres."))
        self.assertFalse(sentenceVerification.sentenceVerification("4676 6787 9246 346779."))
    
    # Verify if there is any type of punctuation in the setence ( ex: comma , semi-colon, interrogation point, colon)
    def test_acceptOtherPunctuation(self):
        self.assertTrue(sentenceVerification.sentenceVerification("Est-ce qu'une question fonctionne ?"))

    # Verify if there any proper noun ( ex: Albert )
    def test_checkProperNoun(self):
        self.asserttrue(sentenceVerification.sentenceVerification("Avec le nom d'Albert, ça fonctionne."))

def main():
    pass

if __name__ == '__main__':
    unittest.main()
