import unittest
import sentenceVerification


class TestSentence(unittest.TestCase):
    # Test if there's an uppercase at the start of the string
    def test_firstLetterUpperCase(self):
        self.assertTrue(sentenceVerification.sentenceVerification("Ceci est une phrase."))
        self.assertFalse(sentenceVerification.sentenceVerification("ceci est une phrase."))

    # Test if the number of words is between 2 and 10 excluded
    def test_numberOfWords(self):
        self.assertFalse(sentenceVerification.sentenceVerification("Ceci est."))
        self.assertTrue(sentenceVerification.sentenceVerification("Ceci est une phrase."))
        self.assertFalse(sentenceVerification.sentenceVerification("Ceci est une grande phrase trop longue pour être valide."))

    # Test if there's a period at the end of the sentence
    def test_periodAtEnd(self):
        self.assertTrue(sentenceVerification.sentenceVerification("Ceci est une phrase."))
        self.assertFalse(sentenceVerification.sentenceVerification("Ceci est une phrase"))

    # NEW COMMENTARIES - POSSIBLE UPGRADES
    """
    # Test is string is empty
    def test_isempty(self):
        self.assertTrue(sentenceVerification.sentenceVerification(""))

    # Verify if there is only numbers in the sentence
    def test_onlyNumbers(self):
        self.assertTrue(sentenceVerification.sentenceVerification("4545 54 587 7 51554545"))

    # Verify if there is any type of punctuation in the sentence except the period at the end ( ex: comma , semi-colon, interrogation point, colon)
    def test_more_punctuation(self):
        self.assertTrue(sentenceVerification.sentenceVerification("Ceci, est; une: phrase ?"))

    # Verify if there any proper noun ( ex: Albert )
    def test_proper_noun(self):
        self.assertTrue(sentenceVerification.sentenceVerification("Ceci est une phrase d'Albert."))
    """

if __name__ == '__main__':
    unittest.main()
